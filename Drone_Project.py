import cv2
from djitellopy import tello
import cvzone

def nothing(x):
    pass

t = time()
i = 0

capture = cv2.VideoCapture(0)
cv2.namedWindow("Window")

cv2.WindowMake("Lower tone", "Window", 0, 100, nothing)
cv2.WindowMake("Lower sat", "Window", 0, 200, nothing)
cv2.WindowMake("Lower value", "Window", 0, 200, nothing)
cv2.WindowMake("Upper tone", "Window", 100, 100, nothing)
cv2.WindowMake("Upper sat", "Window", 150, 200, nothing)
cv2.WindowMake("Upper value", "Window", 200, 200, nothing)
cv2.WindowMake("Break", "Window", 0, 10, nothing)



while True:
    _, frame = cap.read()
    frame = cv2.resize(frame, (0,0), fx=0.3, fy=0.3)
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    l_h = cv2.getTrackbarPos("Lower tone", "Window")
    l_s = cv2.getTrackbarPos("Lower sat", "Window")
    l_v = cv2.getTrackbarPos("Lower value", "Window")
    u_h = cv2.getTrackbarPos("Upper tone", "Window")
    u_s = cv2.getTrackbarPos("Upper sat", "Window")
    u_v = cv2.getTrackbarPos("Upper value", "Window")
    ero = cv2.getTrackbarPos("Break", "Window")

    lower_limit = np.array([l_h, l_s, l_v])
    upper_limit = np.array([u_h, u_s, u_v])
    mask = cv2.inRange(hsv, lower_limit, upper_limit)

    result = cv2.bitwise_and(frame, frame, mask=mask)
    cv2.imshow("result", result)

    key = cv2.waitKey(30)
    if key == 27:
        break

    if i%30 == 0:
        print ("30 frames {}, fps {:.2f}".format(i//30, i/(time() - t)))
    i += 1



print("lower HSV: {}, {}, {}".format(l_h, l_s, l_v) )
print("upper HSV: {}, {}, {}".format(u_h, u_s, u_v) )
print("erode:", ero)

cap.release()
cv2.destroyAllWindows()
