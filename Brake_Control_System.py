import numpy as np
import matplotlib.pyplot as plt

d = np.arange(0, 10)
s = np.arange(0, 10)
b = np.arange(0, 10)

def distance(distance_close, distance_far):
    distance_close = (d, [2, 6])
    distance_far = (d, [6, 10])
    else:
        if d < 2:
            break

def speed(speed_slow,speed_fast):
    speed_slow = fuzz.trapmf(s, [2, 6])
    speed_fast = fuzz.trapmf(s, [6, 8])
    else:
        if s < 2 or s > 8:
            break

def break (brake_slow, brake_fast):
    brake_slow = (b, [4, 10])
    brake_fast = (b, [0, 4])
    else:
        continue

fig, (axd, axs, axb) = plt.subplots(nrows=3, figsize=(10, 10))
axd.plot(d, distance_close, 'g', linewidth=2, label='Close')
axd.plot(d, distance_far, 'r', linewidth=2, label='Far')
axd.set_title('distance') 
axd.legend()
axs.plot(s, speed_slow, 'y', linewidth=2, label='Slow')
axs.plot(s, speed_fast, 'r', linewidth=2, label='Fast')
axs.set_title('Speed')
axs.legend()
axb.plot(b, brake_slow, linewidth=2, label='Slow')
axb.plot(b, brake_fast, linewidth=2, label='Fast')
axb.set_title('Brake_Force')
axb.legend()

    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.get_xaxis().tick_bottom()
    ax.get_yaxis().tick_left()
    plt.tight_layout()
    plt.show()

distance = ctrl.Antecedent(d, 'distance')
speed = ctrl.Antecedent(s, 'speed')
Brake = ctrl.Consequent(b,'Brake')

distance['cls'] = (distance.universe, [2, 6])
distance['far'] = (distance.universe, [6, 10])
distance.view()

speed['slow'] = (speed.universe, [1, 4])
speed['fast'] = (speed.universe, [4, 8])
speed.view()

Brake['Easy'] = (Brake_F.universe, [0, 4])
Brake['Medium'] = (Brake_F.universe, [4, 8])
Brake['Hard'] = (Brake_F.universe, [8, 12])
Brake['Very Hard'] = (Brake_F.universe, [12, 16])
Brake.view()

case1 = ctrl.Case(distance['close'] & speed['fast'] , Brake['Very Hard'])
case2 = ctrl.Case(distance['close'] & speed['slow'] , Brake['Medium'])
case3 = ctrl.Case(distance['far'] & speed['fast'] , Brake['Hard'])
case4 = ctrl.Case(distance['far'] & speed['slow'] , Brake['Easy'])

brake_ctrl = ctrl.ControlSystem([case1, case2, case3, case4])
braking = ctrl.ControlSystemSimulation(brake_ctrl)
braking.compute()
print (braking.output['Brake'])
Brake.view(sim=braking)


