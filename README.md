Project 1
Author - Erik Gyulbudaghyan
About - This project created to compute the data from GPS, and use this cordinates to detect objects from drons.
Prerequisites - First of all for this project we need to understand the main problem which is the method of getting the information and data that we get from GPS. Second we need to understand how can we compare this data that we get from GPS with the data that we got from drons.
Usage - This project in my opinion is very useful as for not professional projects, like university projects, and at the same time as a very good example of solving this solution in number of serius and professional projects, which are mainly have a connection with a military programs.

Project 2
Author - Erik Gyulbudaghyan
About - An programmed brake system that adjusts the brake weight based on inter-vehicle remove and velocity is proposed in this extend. For this project the main part is done by using Python.
Prerequisites - Firstly it was imperative to choose the right way to calculate and they exercise it in Python. It was viral to understand how to correctly calculate it, and as a result ogt the good numbers for our Braking Control system
Usage - In my opinion this project would be good for Dynamics engineers, who are working on creating simulations of an automatical brake systems. Or also this can be used as an example for other students, who are interested to do such project in future.

Project 3
Author - Erik Gyulbudaghyan
About - This project is about to Control inverted pendulum on the moving cart. The framework in this case comprises of an inverted pendulum mounted to a cart. The rearranged pendulum framework is an illustration commonly found in many control systems projects. 
Prerequisites - To find out how to contro the inverted pendulum on the mechanical or not mechanical cart, which is moving at the same time. We first of all need to find the method of stabilazing the pendulum in two dimention which is in our case the main problem. After that we need to find the correct strategy which will alow us to representate our calculation in Python.
Usage - I think that this project is very good project for students tht are going to pass courses that are require the knowledge of Dynamics. This project is a good example of project which can help students to better understanding the Dynamics and its rules.

Project 4
Author - Erik Gyulbudaghyan
About - This project will be utilized to calculate the Work utilizing necessarily numerical strategies by using the Python
Prerequisites - First of all it was imperative to choose which strategy of calculating the numerical part would be the best for getting the best results.  After that we are going to show the best way to get this calculation correctly working in Python.  
Usage - In my opinion this project mostly will be used in not professional projects, like in university projects. This would help students in future to calculate by using this strategy that I show in my python code.
